from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    # url(r'^$', views.base, name='base'),
    url(r'^$', views.authentication, name='login'),
    url(r'^programador$', views.programador, name='programador'),
    url(r'^administrador$', views.administrador, name='administrador'),
    url(r'^perfil$', views.perfil, name='perfil'),
    url(r'^administrador/predios/activos$', views.predios_activos, name='predios_activos'),
    url(r'^logout$', auth_views.logout, {'next_page': '/'}, name='logout'),
]
