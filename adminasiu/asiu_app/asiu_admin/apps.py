from __future__ import unicode_literals

from django.apps import AppConfig


class AsiuAdminConfig(AppConfig):
    name = 'asiu_admin'
