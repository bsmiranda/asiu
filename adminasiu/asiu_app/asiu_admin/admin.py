from django.contrib import admin
from .models import *

# admin.site.register(Base)
# admin.site.register(Conversacion)
# admin.site.register(RespuestaAutomatica)
# admin.site.register(Reprogramacion)
# admin.site.register(SolicitudDeServicios)
# admin.site.register(Tecnico)
# admin.site.register(Servicio)

@admin.register(Base)
class AdminBase(admin.ModelAdmin):
	list_display = ('matricula', 'nombres', 'apellidos', 'direccion1', 'telefono', 'celular', 'ciudad',)

@admin.register(Conversacion)
class AdminConversacion(admin.ModelAdmin):
	list_display = ('fecha', 'hora', 'conversacion',)

@admin.register(RespuestaAutomatica)
class AdminRespuestaAutomatica(admin.ModelAdmin):
	list_display = ('codigo', 'fecha_de_creacion', 'fecha_de_modificacion', 'tecla_1', 'tecla_2', 'tecla_3', 'descripcion',)

@admin.register(Usuario)
class AdminUsuario(admin.ModelAdmin):
	list_display = ('username', 'first_name', 'last_name','tipo', 'email', 'is_staff', 'is_active', 'is_superuser',)

@admin.register(Reprogramacion)
class AdminReprogramacion(admin.ModelAdmin):
	list_display = ('fecha_de_reprogramacion', 'fecha_reprogramada', 'descripcion',)

@admin.register(SolicitudDeServicios)
class AdminSolicitudDeServicios(admin.ModelAdmin):
	list_display = ('fecha_de_solicitud', 'fecha_solicitada', 'descripcion',)

@admin.register(Tecnico)
class AdminTecnico(admin.ModelAdmin):
	list_display = ('nombres', 'apellidos', 'cc', 'direccion', 'telefono', 'celular', 'numero_de_servicios')

@admin.register(Servicio)
class AdminServicio(admin.ModelAdmin):
	list_display = ('descripcion',)