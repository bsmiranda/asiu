from django import forms
from .models import Usuario

class IngresoForm(forms.Form):
    username = forms.CharField(error_messages={'required': 'Espacio Requerido!'})
    password = forms.CharField(widget=forms.PasswordInput(),
           error_messages={'required':'Espacio Requerido!'})