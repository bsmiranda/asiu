from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect, get_object_or_404

from django.contrib.auth.decorators import login_required

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect

from .forms import IngresoForm
# , CambioPwdForm
# from django.contrib.auth import get_user_model
# from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth import authenticate, login

from .models import Base, Usuario, RespuestaAutomatica

# def base(request):
# 	base = Base.objects.order_by('id')
# 	template = loader.get_template('index.html')
# 	title = 'ASIU'
# 	version = "1.0"
# 	invitado = 'Invitado'
# 	context = {
# 		'bases': base,
# 		'title': title,
# 		'version': version,
# 		'invitado': invitado
# 	}

# 	return HttpResponse(template.render(context, request))

'''def authentication(request):
	title = 'ASIU'
	version = "1.0"
	invitado = 'Invitado'

	# User = get_user_model()

	if request.method == 'POST':
		action = request.POST.get('action', None)
		username = request.POST.get('username', None)
		password = request.POST.get('password', None)
		usr = Usuario.objects.get('username', username)
		pw = Usuario.objects.get('password', password)

		print 'ususarios' + usr

		if action == 'login':
			if usr and pw:
				user = authenticate(username=username, password=password)
				login(request, user)

			return redirect('/programador')
		else:
			return redirect('/')

	return render(request, 'index.html', {'title': title, 'version': version, 'invitado': invitado})'''

# ---------------------------------------------------------
# Create your views here.
# def index(request):
# 	return render_to_response('index.html', locals(), context_instance = RequestContext(request))

# def acerca_de(request):
#     return render_to_response('home/acerca_de.html', locals(), context_instance = RequestContext(request))

def authentication(request):
# formulario - msg_no - ver_error - lista_err: se deben llamar asi, el include las referencia con ese nombre
	title = "ASIU"
	version = '1.0'
	invitado = 'Invitado'
	valido= False
	ver_error= False
	msg_no='Ingreso no valido'
	lista_err= []

	if request.method == 'POST':
		formulario = IngresoForm(request.POST)
		valido = formulario.is_valid()
		if valido:
			username = formulario.cleaned_data['username']
			password =  formulario.cleaned_data['password']
			usrLog = Usuario.objects.login_ok(username,password)
			if usrLog != None:
				request.session['username'] = usrLog.username
				user = Usuario.objects.get(username=username)
				# return render(request, 'programador.html', {'title': title, 'version': version, 'invitado': invitado})
				# return HttpResponseRedirect('/programador')
				if user.tipo == "Programador":
					return HttpResponseRedirect('/programador')
				elif user.tipo == "Administrador":
					return HttpResponseRedirect('/administrador')
			else:
				ver_error = True

		else:
			ver_error = True
			for field in formulario:
				for error in field.errors:
					lista_err.append(field.label + ': ' + error)

	else:
		formulario = IngresoForm()

	return render(request, 'index.html', {'title': title, 'version': version, 'invitado': invitado})
    # return render_to_response('index.html', locals(), context_instance = RequestContext(request))

'''def cambio_clave(request):
    return render_to_response('home/cambio_clave.html', locals(), context_instance = RequestContext(request))'''

def salir(request):
    del request.session['username']
    return HttpResponseRedirect('/')

# ---------------------------------------------------------
# @login_required(login_url='programador')

def programador(request):
	if "username" in request.session:
		base = Base.objects.order_by('id')
		resp = RespuestaAutomatica.objects.order_by('id')
		template = loader.get_template('programador.html')

		try:
			user = Usuario.objects.get(username=request.session['username'])
		except Usuario.DoesNotExist:
			user = 'None'

		title = 'ASIU'
		version = "1.0"
		context = {
			'bases': base,
			'resps': resp,
			'user': user,
			'title': title,
			'version': version,
		}

		return HttpResponse(template.render(context, request))
	else:
		return HttpResponseRedirect('/')

def administrador(request):
	if "username" in request.session:
		base = Base.objects.order_by('id')
		template = loader.get_template('administrador.html')
		# users = request.session

		try:
			user = Usuario.objects.get(username=request.session['username'])
		except Usuario.DoesNotExist:
			user = 'None'

		title = 'ASIU'
		version = "1.0"
		context = {
			'bases': base,
			'user': user,
			'title': title,
			'version': version,
		}

		return HttpResponse(template.render(context, request))
	else:
		return HttpResponseRedirect('/')

def perfil(request):
	base = Base.objects.order_by('id')
	# users = request.session

	try:
		user = Usuario.objects.get(username=request.session['username'])
	except Usuario.DoesNotExist:
		user = 'None'

	if user.tipo == "Administrador":
		template = loader.get_template('perfil_admin.html')
	elif user.tipo == "Programador":
		template = loader.get_template('perfil_prog.html')
	title = 'ASIU'
	version = "1.0"
	context = {
		'bases': base,
		'user': user,
		'title': title,
		'version': version,
	}

	return HttpResponse(template.render(context, request))

def predios_activos(request):
	base = Base.objects.order_by('id')
	template = loader.get_template('predios_activos.html')
	# users = request.session

	try:
		user = Usuario.objects.get(username=request.session['username'])
	except Usuario.DoesNotExist:
		user = 'None'

	title = 'ASIU'
	version = "1.0"
	context = {
		'bases': base,
		'user': user,
		'title': title,
		'version': version,
	}

	return HttpResponse(template.render(context, request))