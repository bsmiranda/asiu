from __future__ import unicode_literals

from django.db import models

from django.core.exceptions import ValidationError
from .managers import UsuarioManager

# from django.contrib.auth.models import AbstractBaseUser


# Create your models here.
class Base(models.Model):
	matricula = models.CharField(max_length=30)
	nombres = models.TextField(max_length=50)
	apellidos = models.TextField(max_length=50)
	direccion1 = models.CharField(max_length=255)
	telefono = models.CharField(max_length=10)
	celular = models.CharField(max_length=10)
	ciudad = models.TextField(max_length=255)
	fotografia_del_predio = models.ImageField(upload_to='predios/')

	# def __str__(self):
	# 	return self.matricula

class Conversacion(models.Model):
	fecha = models.DateField()
	hora = models.TimeField()
	conversacion = models.CharField(max_length=255)
	# matricula

class RespuestaAutomatica(models.Model):
	codigo = models.IntegerField()
	fecha_de_creacion = models.DateField()
	hora_de_creacion = models.TimeField()
	fecha_de_modificacion = models.DateField()
	hora_de_modificacion = models.TimeField()
	tecla_1 = models.CharField(max_length=30)
	tecla_2 = models.CharField(max_length=30, blank=True)
	tecla_3 = models.CharField(max_length=30, blank=True)
	nombre = models.CharField(max_length=255, default=False, blank=True)
	descripcion = models.CharField(max_length=255)
	# nombre_de_usuario

'''class Usuario(models.Model):
	tipo = models.CharField(max_length=20)
	nombres = models.TextField(max_length=255)
	apellidos = models.TextField(max_length=255)
	fotografia = models.ImageField()'''

'''class Usuario(AbstractBaseUser):
	username = models.CharField(("username"), unique=True, max_length=30, default=False)
	tipo = models.CharField(max_length=20)
	email = models.EmailField('email address', unique=True, db_index=True, default=False)
	is_staff = models.BooleanField('is staff', default=False)
	is_active = models.BooleanField('is active', default=False)
	is_superuser = models.BooleanField('is superuser', default=False)
	first_name = models.TextField('first name', default=None, null=True)
	last_name = models.TextField('last name', default=None, null=True)
	avatar = models.ImageField('profile picture', upload_to='avatars/', null=True, blank=True)

	USERNAME_FIELD = 'username'''

class Usuario(models.Model):
	username = models.CharField(unique=True, max_length=30, default=False)
	password = models.CharField(unique=True, max_length=30, default=False)
	first_name = models.TextField(default=None, null=True)
	last_name = models.TextField(default=None, null=True)
	tipo = models.CharField(max_length=20)
	email = models.EmailField(unique=True, db_index=True, default=False)
	last_login = models.DateField()
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=False)
	is_superuser = models.BooleanField(default=False)
	date_joined = models.DateField()
	avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)

	objects = UsuarioManager()

class Reprogramacion(models.Model):
	fecha_de_reprogramacion = models.DateField()
	hora_de_reprogramacion = models.TimeField()
	fecha_reprogramada = models.DateField()
	hora_reprogramada = models.TimeField(blank=True)
	descripcion = models.CharField(max_length=255)
	# matricula

class SolicitudDeServicios(models.Model):
	fecha_de_solicitud = models.DateField()
	hora_de_solicitud = models.TimeField()
	fecha_solicitada = models.DateField()
	hora_solicitada = models.TimeField(blank=True)
	descripcion = models.CharField(max_length=255)
	# matricula

class Tecnico(models.Model):
	nombres = models.TextField(max_length=255)
	apellidos = models.TextField(max_length=255)
	direccion = models.CharField(max_length=255)
	telefono = models.CharField(max_length=10)
	celular = models.CharField(max_length=10)
	fotografia = models.ImageField()
	cc = models.IntegerField()
	numero_de_servicios = models.IntegerField()

class Servicio(models.Model):
	descripcion = models.CharField(max_length=255)
	# nombres_tecnico
	# apellidos_tecnico
